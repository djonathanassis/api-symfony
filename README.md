# Desafio Programador PHP

Teste técnico para [Brasil TecPar](https://www.brasiltecpar.com.br/) 

O teste é formado por 3 partes.

1. A criação de uma rota que encontra um hash, de certo formato, para uma certa string fornecida como input.
   - [x] Criação de rota.
   - [x] Encontro da key que gere hash iniciando com 4 zeros para certa string informada.
   - [x] Controle do número máximo de requisições aceitas em 1 minuto pela rota.
2. A criação de um comando que consulta a rota criada e armazena os resultados na base de dados.
    - [x] Buscar e armazenar resultados obtidos.
    - [x] Respeitar o limite de requisições da rota.
    - [x] Aguardar o menor tempo possível para realização de todas requisições solicitadas.
3. Criação de rota que retorne os resultados que foram gravados.
    - [x] Retornar os resultados de forma paginada;
    - [x] Ter o filtro por "Número de tentativas" podendo filtrar por resultados que tiveram menos de x tentativas.
    - [x] Não devem ser retornados todos os campos da tabela, somente as informações nas colunas batch, "Número do bloco", "String de entrada" e "Chave encontrada".

## Execução local com Docker



Com a **configuração inicial** já realizada, suba os containers se necessário e acesse a aplicação em `localhost:8081`

```sh
docker-compose up -d
```

#### API

Acesse as rotas disponível em `localhost:8081/api/hash`, `localhost:8081/api/hash/list`

