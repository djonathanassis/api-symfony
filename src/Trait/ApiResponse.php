<?php

namespace App\Trait;

use Symfony\Component\HttpFoundation\JsonResponse;

trait ApiResponse
{

    protected function response($data, int $code = 200, string $message = null): JsonResponse
    {
        return (new JsonResponse(
            [
                'status' => $code === 2 ? 'Success' : 'Warning',
                'message' => $message,
                'data' => $data
            ]))
            ->setStatusCode($code);
    }

    protected function errorResponse($data, string $message, int $code): JsonResponse
    {
        return (new JsonResponse(
            [
                'status' => 'Error',
                'message' => $message,
                'data' => $data
            ]))
            ->setStatusCode($code);
    }


}