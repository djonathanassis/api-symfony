<?php

namespace App\Entity;

use App\Repository\HashRepository;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Internal\TentativeType;

#[ORM\Entity(repositoryClass: HashRepository::class)]
class Hash
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: "datetime")]
    private mixed $batch;

    #[ORM\Column(type: 'string', length: 100)]
    private string $string_input;

    #[ORM\Column(type: 'string', length: 100)]
    private string $key_found;

    #[ORM\Column(type: 'integer', nullable: 'false')]
    private int $number_attempts;

    #[ORM\Column(type: 'string', length: 100)]
    private string $hash_generated;

    public function __construct(
        \DateTime $batch,
        string $string_input,
        string $key_found,
        string $hash_generated,
        int $number_attempts
    )
    {
        $this->batch = $batch;
        $this->string_input = $string_input;
        $this->key_found = $key_found;
        $this->hash_generated = $hash_generated;
        $this->number_attempts = $number_attempts;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime|mixed
     */
    public function getBatch(): mixed
    {
        return $this->batch;
    }

    /**
     * @return string
     */
    public function getStringInput(): string
    {
        return $this->string_input;
    }

    /**
     * @return string
     */
    public function getKeyFound(): string
    {
        return $this->key_found;
    }

    /**
     * @return int
     */
    public function getNumberAttempts(): int
    {
        return $this->number_attempts;
    }

    /**
     * @return string
     */
    public function getHashGenerated(): string
    {
        return $this->hash_generated;
    }


    public function toArray()
    {
        return [
            'batch'           => $this->batch->format('Y-d-m H:i:s'),
            'string_input'    => $this->string_input,
            'key_found'       => $this->key_found,
            'hash_generated'  => $this->hash_generated,
            'number_attempts' => $this->number_attempts
        ];
    }
}
