<?php

namespace App\Service;

use App\Entity\Hash;
use App\Repository\HashRepository;
use Doctrine\ORM\Query;
use Symfony\Component\HttpClient\Exception\JsonException;


class HashService
{
    public function __construct(
        protected HashRepository $hashRepository
    )
    {
    }

    public function listHash($number_Attempts = null): Query
    {
        return $this->hashRepository->listAllFilter($number_Attempts);
    }

    /**
     */
    public function createHash($string): array|JsonException
    {
        $hashFound = $this->hashRepository->findHashByInput($string);

        if ($hashFound){
            return new JsonException('This string has already been registered', 404);
        }

        $calculateHash = $this->generateHash($string);

        $hash = new Hash(
            $calculateHash->batch,
            $calculateHash->string_input,
            $calculateHash->key_found,
            $calculateHash->hash_generated,
            $calculateHash->number_attempts,
        );

        $this->hashRepository->add($hash, true);

        return $hash->toArray();
    }

    protected function generateHash($string): object|array
    {
        $lock = true;

        $currentAttempt = 0;
        $hashResult = [];
        while ($lock) {

            $hashKeyEightDigits = $this->HashKeyEightDigits();
            $hashMD5 = md5($string . $hashKeyEightDigits);

            if ($this->isCheckHash($hashMD5)) {
                $lock = false;
                $hashResult = (object)[
                    'batch'           => (new \DateTime("now")),
                    'string_input'    => $string,
                    'key_found'       => $hashKeyEightDigits,
                    'hash_generated'  => $hashMD5,
                    'number_attempts' => $currentAttempt
                ];
            }
            $currentAttempt++;
        }

        return $hashResult;
    }

    public function setRowsHashes(array $hashes): array
    {
        $rows = [];

        foreach ($hashes as $hash) {
            $rows[] = [
                $hash->getBatch()->format('Y-d-m H:i:s'),
                $hash->getId(),
                $hash->getStringInput(),
                $hash->getKeyFound(),
                $hash->getHashGenerated(),
                $hash->getNumberAttempts()
            ];
        }
        return $rows;
    }

    private function isCheckHash(string $hashMD5): bool
    {
        return (str_starts_with($hashMD5, '0000'));
    }

    private function HashKeyEightDigits(): string
    {
        $str = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($str), 0, 8);
    }

}