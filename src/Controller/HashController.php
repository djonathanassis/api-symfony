<?php

namespace App\Controller;

use App\Service\HashService;
use App\Trait\ApiResponse;
use JMS\Serializer\SerializerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/api", name:"api_")]
class HashController extends AbstractController
{
    use ApiResponse;

    public function __construct(
        protected HashService $hashService
    )
    {
    }

    #[Route('/hash/list', name: 'hash_index', methods: 'GET')]
    public function index(Request $request, PaginatorInterface $paginator, SerializerInterface $serializer): JsonResponse
    {
        $numberAttempts = (int) $request->query->get('number-attempts');
        $pege = $request->query->get('page');
        $limit = $request->query->get('limit');

        try {
            if ($numberAttempts){
                $response = $this->hashService->listHash($numberAttempts)->getArrayResult();
            } else {
                $response = $paginator->paginate($this->hashService->listHash(), $pege, $limit);
            }

            return $this->successResponse
            (
                $serializer->toArray($response),
                Response::HTTP_CREATED,
                false
            );

        } catch (\ErrorException $exception){
            return $this->errorResponse
            (
                false,
                $exception->getMessage(),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    #[Route('/hash', name: 'hash_store')]
    public function store(Request $request): JsonResponse
    {
        try {
            $string = $request->query->get('string');
            $response = $this->hashService->createHash($string);


            return $this->response
            (
                $response,
                $response->getCode() ?? Response::HTTP_CREATED,
                $response->getMessage() ?? 'String successfully registered.'
            );

        } catch (\ErrorException $exception) {
            return $this->errorResponse
            (
                false,
                $exception->getMessage(),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
